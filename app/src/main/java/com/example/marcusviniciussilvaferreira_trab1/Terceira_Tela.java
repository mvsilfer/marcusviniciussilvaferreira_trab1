package com.example.marcusviniciussilvaferreira_trab1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Terceira_Tela extends AppCompatActivity {

    Button botaoHome3,botaoConfig3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terceira__tela);

        botaoHome3=findViewById(R.id.btnHome3);
        botaoConfig3=findViewById(R.id.btnCadastro3);

        botaoHome3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tela1= new Intent(getApplicationContext(),MainActivity.class);
                startActivity(tela1);
            }
        });

        botaoConfig3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tela2= new Intent(getApplicationContext(),Segunda_Tela.class);
                startActivity(tela2);
            }
        });
    }
}