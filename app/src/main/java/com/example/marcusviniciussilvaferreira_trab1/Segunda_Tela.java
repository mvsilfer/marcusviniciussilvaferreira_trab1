package com.example.marcusviniciussilvaferreira_trab1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Segunda_Tela extends AppCompatActivity {

    Button botaoHome2,botaoConfig2,botaoEnviar;
    TextView txtNomeCa;
    TextView txtTelefoneCa;
    TextView txtEmailCa;
    TextView txtSexoCa;

    ImageButton btnCamera;
    ImageView imgCamera;
    String currentPhotoPath;
    Uri uriSaveImage;

    private static final int REQUEST_IMAGE_CAPTURE = 2;

    private File createImageFile()throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Bitmap imageBitmap = null;
            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriSaveImage);
                imgCamera.setVisibility(View.VISIBLE);
                imgCamera.setImageBitmap(imageBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
            imgCamera.setImageBitmap(imageBitmap);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_segunda__tela);

        botaoHome2=findViewById(R.id.btnHome2);
        botaoConfig2=findViewById(R.id.btnConfig2);

        txtNomeCa = (TextView) findViewById(R.id.txtNomeCa);
        String nome = getIntent().getStringExtra("nome");
        txtNomeCa.setText(nome);


        txtSexoCa = (TextView) findViewById(R.id.txtSexoCa);
        String sexo = getIntent().getStringExtra("sexo");
        txtSexoCa.setText(sexo);

        txtEmailCa = (TextView) findViewById(R.id.txtEmailCa);
        String email = getIntent().getStringExtra("email");
        txtEmailCa.setText(email);

        txtTelefoneCa = (TextView) findViewById(R.id.txtTelefoneCa);
        String telefone = getIntent().getStringExtra("telefone");
        txtTelefoneCa.setText(telefone);


        String textocapturado   = getIntent().getStringExtra(Intent.EXTRA_TEXT);

        btnCamera = (ImageButton) findViewById(R.id.btnCamera);
        imgCamera = (ImageView) findViewById(R.id.imgCamera);

        botaoHome2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tela1= new Intent(getApplicationContext(),MainActivity.class);
                startActivity(tela1);
            }
        });

        botaoConfig2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tela3= new Intent(getApplicationContext(),Terceira_Tela.class);
                startActivity(tela3);
            }
        });


        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Context context = getApplicationContext();
                try {
                    uriSaveImage = FileProvider.getUriForFile(context,"br.com.jncrlmnds.ciclovidaactivity.fileprovider",createImageFile());
                    Log.i("Segunda_Tela", "Caminho da imagem = " + uriSaveImage.toString());

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,uriSaveImage);

                    if (intent.resolveActivity(getPackageManager()) != null)
                    {
                        startActivityForResult(intent,REQUEST_IMAGE_CAPTURE);

                    }
                }  catch (IOException e) {

                    e.printStackTrace();

                }



            }
        });



        String currentPhotoPath;



        botaoEnviar = (Button) findViewById(R.id.botaoEnviar);
        botaoEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_TEXT, "Nome: " + txtNomeCa.getText().toString()  +"\n"+  "Telefone: " +txtTelefoneCa.getText().toString()  +"\n"+ "Email: " +  txtEmailCa.getText().toString() +"\n"+ "Sexo: " + txtSexoCa.getText().toString());
                intent.putExtra(Intent.EXTRA_STREAM, uriSaveImage);
                intent.setType("text/plain");
                startActivity(intent);

            }
        });


    }
}