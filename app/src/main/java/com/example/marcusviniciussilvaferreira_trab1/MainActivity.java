package com.example.marcusviniciussilvaferreira_trab1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button botaoCadastro1,botaoConfig1, botaoPerfil1;
    private EditText txtNome;
    private EditText txtTelefone;
    private EditText txtEmail;
    private EditText txtSexo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botaoCadastro1=findViewById(R.id.btnCadastro1);
        botaoConfig1=findViewById(R.id.btnConfig1);
        botaoPerfil1=findViewById(R.id.btnPerfil1);

        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtTelefone = (EditText) findViewById(R.id.txtTelefone);
        txtNome = (EditText) findViewById(R.id.txtNome);
        txtSexo = (EditText) findViewById(R.id.txtSexo);

        botaoCadastro1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String nome = txtNome.getText().toString();
                String sexo = txtSexo.getText().toString();
                String email = txtEmail.getText().toString();
                String telefone = txtTelefone.getText().toString();
                Intent intent = new Intent(getApplicationContext(),Segunda_Tela.class);
                intent.putExtra("nome",txtNome.getText().toString());
                intent.putExtra("sexo",txtSexo.getText().toString());
                intent.putExtra("email",txtEmail.getText().toString());
                intent.putExtra("telefone",txtTelefone.getText().toString());

                startActivity(intent);
                finish();
            }
        });

        botaoConfig1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tela3= new Intent(getApplicationContext(),Terceira_Tela.class);
                startActivity(tela3);
            }
        });

        botaoPerfil1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tela4 = new Intent(getApplicationContext(),Quarta_Tela.class);
                startActivity(tela4);
            }
        });

    }
}